﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Timers;

namespace TravelLeaders
{
    public partial class Service1 : ServiceBase
    {
        private Timer timer1 = null;
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            timer1 = new Timer();
            this.timer1.Interval = 60000*60*24;
            //  this.timer1.Interval = 300000;
            timer1.Enabled = true;
            this.timer1.Elapsed += new System.Timers.ElapsedEventHandler(Tick);
            Library.WriteErrorLogs("Started");
        }

        private void Tick(object sender, ElapsedEventArgs e)
        {
            try
            {
                CorporateInvoices cs = new CorporateInvoices();
                cs.FillInvoiceMain();
                CorporateDetails de = new CorporateDetails();
                de.InvoiceDetails();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        protected override void OnStop()
        {
            Library.WriteErrorLogs("Stopped");
        }
    }

    public static class Library
    {
        public static void WriteErrorLog(Exception ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\logfile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.Source.ToString() + " " + ex.Message.ToString());
                sw.Flush();
                sw.Close();
            }
            catch (Exception err)
            {
                throw err;
            }
        }
        public static void WriteErrorLogs(string ex)
        {
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter(AppDomain.CurrentDomain.BaseDirectory + "\\logfile.txt", true);
                sw.WriteLine(DateTime.Now.ToString() + " " + ex.ToString());
                sw.Flush();
                sw.Close();
            }
            catch (Exception err)
            {
                throw err;
            }
        }
    }
}

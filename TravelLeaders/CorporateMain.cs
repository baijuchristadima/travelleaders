﻿using System.Text;
using System.Data;

using System;
using Npgsql;
using MySql.Data.MySqlClient;

namespace TravelLeaders
{
    class CorporateInvoices
    {
        public MySqlConnection connection;
        private string server;
        private string database;
        private string uid;
        private string password;
        public void FillInvoiceMain()
            {
            try
            {
                //Library.WriteErrorLogs("Started the Execution Corporate Invoices");
                server = "localhost";
                database = "ntt";
                uid = "root";
                password = "Nirvana$123#";
                string connectionString;
                connectionString = "SERVER=" + server + ";" + "DATABASE=" +
                database + ";" + "UID=" + uid + ";" + "PASSWORD=" + password + ";";
                connection = new MySqlConnection(connectionString);
                DataTable tradataTable = new DataTable();
               // string connstring1 = String.Format("Server={0};Port={1};User Id={2};Password={3};Database={4};","10.0.0.8", "5432", "nirvana","nirvana123", "traacs_wave");
                  string connstring1 = String.Format("Server={0};Port={1};User Id={2};Password={3};Database={4};","13.79.31.82", "5432", "nirvana","nirvana123", "traacs_wave_auh");
                StringBuilder traquery2 = new StringBuilder();
                traquery2.Append(getQuery());
               // Library.WriteErrorLogs("traq" + traquery2.ToString());
                //traquery2.Append("select distinct vchr_lpo_no,lpo_date,vchr_document_no,dat_document,case when vchr_customer_code='600139' then 2 else case when vchr_customer_code='600073' then 3 else case when vchr_customer_code='500066' then 1 else 4 end end end  as CompanyId from view_for_sale_and_refund_supplier_wise where (vchr_customer_code='600139' or vchr_customer_code='600073' or vchr_customer_code='600122' or vchr_customer_code='500066') and length(vchr_document_no)>2");
                using (NpgsqlConnection conn2 = new NpgsqlConnection(connstring1))
                using (NpgsqlCommand cmd2 = new NpgsqlCommand(traquery2.ToString(), conn2))
                {
                    conn2.Open();
                    conn2.ClearPool();
                    using (NpgsqlDataReader reader1 = cmd2.ExecuteReader())
                    {
                        tradataTable.Load(reader1);

                        if (tradataTable.Rows.Count > 0)
                        {
                           // Library.WriteErrorLogs(tradataTable.Rows.Count.ToString());
                           // Library.WriteErrorLogs("" + tradataTable.Rows.Count);
                            for (int i = 0; i < tradataTable.Rows.Count; i++)
                            {
                                DateTime? Lpodate;
                                DateTime? verifiedDate = null;
                                if (!DBNull.Value.Equals(tradataTable.Rows[i]["lpo_date"]))
                                {
                                    Lpodate = Convert.ToDateTime(tradataTable.Rows[i].ItemArray[1]);
                                }
                                else
                                {
                                    Lpodate = null;
                                }
                                string SQL = "select invoiceno from ntt.corporateinvoices where invoiceno='" + tradataTable.Rows[i].ItemArray[2].ToString() + "'";
                                //Library.WriteErrorLogs(tradataTable.Rows[i].ItemArray[2].ToString());
                                DataTable dtcorpext = getData(SQL);
                                if (dtcorpext.Rows.Count == 0)
                                {
                                   // Library.WriteErrorLogs(dtcorpext.Rows.Count.ToString());
                                    InsertData(Convert.ToInt64(tradataTable.Rows[i].ItemArray[4]), tradataTable.Rows[i].ItemArray[0].ToString(), Lpodate, tradataTable.Rows[i].ItemArray[2].ToString(), Convert.ToDateTime(tradataTable.Rows[i].ItemArray[3]), tradataTable.Rows[i].ItemArray[5].ToString(), verifiedDate);
                                   // Library.WriteErrorLogs("" + tradataTable.Rows[i].ItemArray[2].ToString());
                                }
                                else
                                {
                                    UpdateData(tradataTable.Rows[i].ItemArray[5].ToString(), dtcorpext.Rows[0].ItemArray[0].ToString(), tradataTable.Rows[i].ItemArray[0].ToString());
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private DataTable getData(string SQL)
        {
            try
            {
                DataTable table = new DataTable();
                connection.Open();
                MySqlCommand cmd = new MySqlCommand(SQL, connection);
                MySqlDataReader dataReader = cmd.ExecuteReader();
                table.Load(dataReader);
                return table;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                connection.Close();
            }
        }

        private string getQuery()
        {
            string Query = "";
            string from = DateTime.Now.AddDays(-10).ToString("yyyy-MM-dd");
            string to = DateTime.Now.ToString("yyyy-MM-dd");
            try
            {
                DataTable dt = getData("SELECT companyid,compnycode FROM ntt.corporatecompanies where status=1 and companyid >51");
               // Library.WriteErrorLogs("count" + dt.Rows.Count.ToString());
                if (dt.Rows.Count >= 1)
                {
                    string case1 = "case", where = "";
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        case1 = case1 + string.Format(" when vchr_customer_code='{0}' then {1} ", dt.Rows[i].ItemArray[1].ToString(), dt.Rows[i].ItemArray[0].ToString());
                        where = where + string.Format(" vchr_customer_code='{0}' or", dt.Rows[i].ItemArray[1].ToString());
                    }
                    where = where.Remove(where.Length - 2);
                    Query = string.Format("select distinct vchr_lpo_no,lpo_date,vchr_document_no,dat_document,{0} end  as CompanyId,case when length(unique_file_name)>2 then unique_file_name else '*.' end as attachment from view_for_sale_and_refund_supplier_wise where({1}) and length(vchr_document_no)>2 and dat_document between '"+ from + "' and '"+ to + "'", case1, where);
                    //Query = string.Format("select distinct vchr_lpo_no,lpo_date,vchr_document_no,dat_document,{0} end  as CompanyId,case when length(unique_file_name)>2 then unique_file_name else '*.' end as attachment from view_for_sale_and_refund_supplier_wise where({1}) and length(vchr_document_no)>2 and vchr_document_no in ('INV/HQ/21051655')", case1, where);
                    // Library.WriteErrorLogs("query" + Query.ToString());
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return Query;
        }
        private void InsertData(Int64 CompanyId, string lpo, DateTime? LpoDate, string invoiceNo, DateTime invoiceDate, string attachpath, DateTime? verifiedDate, int verifiedby = 0, int ispaid = 0, int status = 0, int batch = 0)
        {
            try
            {
                //string SQL = "Insert into ntt.corporateinvoices (companyid,lpo,lpodate,invoiceno,invoicedate,verifiedby,verifiedon,ispaid,status,attachpath,batch) values("+ CompanyId +",'"+ lpo.ToString() +"',"+ LpoDate +",'"+ invoiceNo.ToString() +"',"+ invoiceDate +","+verifiedby+","+ verifiedDate +","+ispaid+","+status +","+ attachpath + ","+ batch +")";
                // string SQL = "Insert into ntt.corporateinvoices (companyid,lpo,lpodate,invoiceno,invoicedate,verifiedby,verifiedon,ispaid,status,attachpath,batch) values('{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11}')",(CompanyId,lpo,LpoDate,invoiceNo.ToString(),invoiceDate,verifiedby,verifiedDate,ispaid,status,attachpath,batch)";                    

                connection.Open();
                string SQL = "INSERT INTO corporateinvoices(companyid,lpo,lpodate,invoiceno,invoicedate,verifiedby,verifiedon,ispaid,status,attachpath,batch) VALUES (@companyid,@lpo,@lpodate,@invoiceno,@invoicedate,@verifiedby,@verifiedon,@ispaid,@status,@attachpath,@batch)";

                MySqlCommand command = new MySqlCommand(SQL, connection);
                command.Parameters.AddWithValue("@companyid", CompanyId);
                command.Parameters.AddWithValue("@lpo", lpo.ToString());
                command.Parameters.AddWithValue("@lpodate", LpoDate);
                command.Parameters.AddWithValue("@invoiceno", invoiceNo.ToString());
                command.Parameters.AddWithValue("@invoicedate", invoiceDate);
                command.Parameters.AddWithValue("@verifiedby", 0);
                command.Parameters.AddWithValue("@verifiedon", verifiedDate);

                command.Parameters.AddWithValue("@ispaid", 1);
                command.Parameters.AddWithValue("@status", 1);
                command.Parameters.AddWithValue("@attachpath", attachpath);
                command.Parameters.AddWithValue("@batch", 0);
                command.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                //Library.WriteErrorLogs(ex.ToString());

            }
            finally
            {
                connection.Close();
            }
        }
        private void UpdateData(string attachpath, string invoiceno,string lpo)
        {
            try
            {
                connection.Open();
                string SQL = "update ntt.corporateinvoices set lpo=@lpo, attachpath = @attachpath where invoiceno = @invoiceno";
                MySqlCommand command = new MySqlCommand(SQL, connection);
                command.Parameters.AddWithValue("@attachpath", attachpath);
                command.Parameters.AddWithValue("@lpo", lpo);
                command.Parameters.AddWithValue("@invoiceno", invoiceno);
                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                //Library.WriteErrorLogs(ex.ToString());
            }
            finally
            {
                connection.Close();
            }
        }
    }

}
